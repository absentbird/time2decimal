package main

import (
    "bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

func printHours(minutes float64) {
	value := minutes / float64(60)
	fmt.Println(value)
}

func main() {
	if len(os.Args) < 2 {
        scanner := bufio.NewScanner(os.Stdin)
        for scanner.Scan() {
            os.Args = append(os.Args, scanner.Text())
        }
    }
	if len(os.Args) < 2 {
        fmt.Println("No time provided.")
		os.Exit(1)
	}
	mins := []float64{}
	for i := 1; i < len(os.Args); i++ {
		timeString := os.Args[i]
        if timeString == "" {
            continue
        }
		if _, err := strconv.ParseFloat(timeString, 64); err == nil {
			timeString = timeString + "m"
		}
		parts := strings.Split(timeString, ":")
		for n, p := range parts {
			if p == "" {
				p = "0"
				parts[n] = p
			}
		}
		switch len(parts) {
		case 2:
			timeString = parts[0] + "m" + parts[1] + "s"
		case 3:
			timeString = parts[0] + "h" + parts[1] + "m" + parts[2] + "s"
		}
		d, err := time.ParseDuration(timeString)
		if err != nil {
			fmt.Println("Could not parse time: " + os.Args[i])
			os.Exit(1)
		}
		mins = append(mins, d.Minutes())
	}
	if len(mins) == 1 {
		printHours(mins[0])
		return
	} else {
		var total float64
		for _, min := range mins {
			printHours(min)
			total += min
		}
		fmt.Println("---")
		printHours(total)
	}
}
