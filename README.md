# Time 2 Decimal
Turn all sorts of times into accurate fractions of hours, then add them together.

## Inputs
Time is accepted in the following formats:
* `1h23m4.5s` time with labels.
* `1:23:4.5` time with colons.
* `83.075` decimal value of minutes.

When using colons, the default is minutes:seconds (e.g. `83:4.5`)  
An empty value is substituted with a zero.

For hours:minutes, simply add a trailing colon: `1:23.075:`  
Likewise, for seconds alone, lead with a single colon: `:4984.5`

## Output
The output is the same duration of time, converted to a decimal number of hours.

If a single time is provided, a single number will be returned.

If more than one time is provided, they will each be converted and returned.  
The total will then be returned after a line of three dashes ( e.g. `---`)

## Example
Input: `t2d 1h23m4.5s 1:23:4.5 83.075`

Output:
```
1.3845833333333333
1.3845833333333333
1.3845833333333333
---
4.1537500000000005
```
